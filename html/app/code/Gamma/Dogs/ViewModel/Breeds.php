<?php


namespace Gamma\Dogs\ViewModel;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;


class Breeds implements ArgumentInterface
{
    /**
     * @var \Gamma\Dogs\Model\Dogapi
     */
    protected $dogapi;

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(
        \Gamma\Dogs\Model\Dogapi $dogapi,
        RequestInterface $request
    )
    {
        $this->dogapi = $dogapi;
        $this->request = $request;
    }

    public function woof()
    {
        $requestedDog = $this->request->getParam('doggy');
        if ($requestedDog) {
            try {
                return $this->dogapi->getDogs($requestedDog);
                //return $requestedDog;
            } catch (Exception $e) {
                return null;
            }
        }
        return null;
    }

    public function woofAll(){
        return $this->dogapi->getAllDogs();
    }

}