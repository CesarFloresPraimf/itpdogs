<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\Data\DogsInterface;
use Gamma\Dogs\Api\Data\DogsInterfaceFactory;
use Gamma\Dogs\Api\DogapiInterface;

class Dogapi implements DogapiInterface
{
    /**
     * @var Connection
     */
    protected $connection;
    /**
     * @var ConnectionBooks
     */
    protected $connectionBooks;

    /**
     * @var DogsInterfaceFactory
     */
    protected $dogsInterfaceFactory;


    public function __construct(
        ConnectionInterface $connection,
        ConnectionInterface $connectionBooks,
        DogsInterfaceFactory $dogsInterfaceFactory
    )
    {
        $this->connection = $connection;
        $this->connectionBooks = $connectionBooks;
        $this->dogsInterfaceFactory = $dogsInterfaceFactory;
    }

    public function getDogs($name): DogsInterface
    {
        $dogData = $this->connection->get("https://dog.ceo/api/breed/{$name}/images/random");
        $dogData2 = $this->connection->get("https://dog.ceo/api/breed/{$name}/list");
        $dogData3 = $this->connection->get("https://openlibrary.org/search.json?title={$name}");
        if(count($dogData3['docs']) > 4){
            $books = array(
                array($dogData3['docs'][0]['title'], "http://covers.openlibrary.org/b/olid/{$dogData3['docs'][0]['edition_key'][0]}-M.jpg", "http://openlibrary.org/books/{$dogData3['docs'][0]['edition_key'][0]}"),
                array($dogData3['docs'][1]['title'], "http://covers.openlibrary.org/b/olid/{$dogData3['docs'][1]['edition_key'][0]}-M.jpg", "http://openlibrary.org/books/{$dogData3['docs'][1]['edition_key'][0]}"),
                array($dogData3['docs'][2]['title'], "http://covers.openlibrary.org/b/olid/{$dogData3['docs'][2]['edition_key'][0]}-M.jpg", "http://openlibrary.org/books/{$dogData3['docs'][2]['edition_key'][0]}")
            );
        } else {
            $books = array("Not Found");
        }
        $dog = $this->dogsInterfaceFactory->create();
        if(gettype($dogData2['message']) == 'string'){
            $sub = array('Not found');
        } else {
            $sub = $dogData2['message'];
        }
        $dog->setImage($dogData['message'])
            ->setName($name)
            ->setSub($sub)
            ->setBooks($books)
        ;
        return $dog;

    }

    public function getAllDogs(): array
    {
        $dogData =  $this->connection->get("https://dog.ceo/api/breeds/list/all");
        $arrayOfDogs = array_keys($dogData['message']);
        $dogList = array();
        foreach ($arrayOfDogs as $doggy) {
            array_push($dogList, $doggy);
        }
        return $dogList;
    }



}